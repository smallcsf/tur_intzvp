﻿#ifndef MIGALO_H
#define MIGALO_H

#include <QThread>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QVector>
#include <QTimerEvent>
#include <QTimer>
#include <QWaitCondition>
#include <QQueue>
#include <time.h>
#include <QSet>

struct lamp
{
    int t_id;
    int period;
    bool rand;
};

enum rezhim { odin, grup };

class Migalo : public QObject
{
    Q_OBJECT
private:

    rezhim rezh;
    int s_grp;
    void reset();
    int r_lamp;
    int getRandTime();
    QString PortName;
    QQueue < int > loff;
    QQueue < int > och_rand;
    int max_rand;
    int min_rand;
    void MakeRandGrpQ();
public:
    explicit Migalo(QObject *parent = 0);
    void SetLampMig(int NLamp, int period);
    void SetComPort(int NComPort);
    void stat(int t_max_rand=1000);
    void stop();
    void setRezhimOdin();
    void setRezhimGrup(int size_grps);
    void SendZeroNLamp();

signals:
    void Mignulo(int NLamp);
    void Migai(int NLamp);
    void test_info(int tst);
    
public slots:
    void DoMigOn(int NLamp);
    void DoMigOff();
    
protected:
    int time_on;
    virtual void SetVLine(int line, int V);
    QVector < lamp > Lamps;
    void timerEvent(QTimerEvent *event);
};

#endif // MIGALO_H
