﻿#include "obregg.h"

ObrEGG::ObrEGG(QObject *parent) :
    QObject(parent), max_elektrds(31)
{
    reset();
}

void ObrEGG::reset()
{
    Fsave = true;
    Datas.resize(0);
    Deb_Dts.clear();
    TimeCop=0;
    test=0;
    k_tik=0;
    k_do=0;
    def_name="";
    NLampTrue=0;
    crit_cop=1000000;
    sposb=sum;
    Debug_out=false;
//    use_elektrds.insert(21);
    qRegisterMetaType<TDatas>("TAllDatas");
    connect(this,SIGNAL(GoSave(TAllDatas)),this,SLOT(FastSave(TAllDatas)));
}

void ObrEGG::SetTimeCop(int TC)
{
    TimeCop=TC;
}

void ObrEGG::SetDName(QString dname)
{
    def_name=dname;
}

void ObrEGG::setCritCop(int cr_cp, int n_lmps)
{
    crit_cop=cr_cp*500*n_lmps;
}

void ObrEGG::setNLamp(int nlmp)
{
    NLampTrue=nlmp;
}

void ObrEGG::SaveDatas_0(QString fname)
{
    QFile file(fname);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    if (Datas.size()>0)
        for (int j=0;j<Datas[0][0].size();++j)
        {
            for (int i=0;i<Datas[0].size();++i)
                out << Datas[0][i][j] << ";" ;
            out << endl ;
        }

    file.close();
}

void ObrEGG::CmplxToNumbEnd(int numb, QVector < TComplex > cmplxs)
{
    int last_id = (Datas[numb].size());
    Datas[numb].resize(last_id+1);
    for (int i=0;i<cmplxs.size();++i)
        Datas[numb][last_id].push_back(cmplxs[i].real);
}

void ObrEGG::CmplxToNumbEnd(int numb, QVector < TComplex > cmplxs, TAllDatas &CpDatas)
{
    int last_id = (CpDatas[numb].size());
    CpDatas[numb].resize(last_id+1);
    for (int i=0;i<cmplxs.size();++i)
        CpDatas[numb][last_id].push_back(cmplxs[i].real);
}

void ObrEGG::PreSaveObr()
{
    for (int i=0;i<Datas.size();++i)
        if (Datas[i].size()>0)
            for (int j=0;j<Datas[i][0].size();++j)
                for (int k=0;k<(Datas[i].size()-1);++k)
                    Datas[i][k][j]=0.25*Datas[i][k][j]/Datas[i][Datas[i].size()-1][j];

    for (int i=0;i<Datas.size();++i)
        if (Datas[i].size()>0)
            for (int k=0;k<(Datas[i].size()-1);++k)
                for (int j=(Datas[i][k].size()-1);j>=0;--j)
                    Datas[i][k][j]-=Datas[i][k][0] ;

    int end_Datas=Datas.size();
    for (int i=0;i<end_Datas;++i)
        if (Datas[i].size()>0)
        {
            int end_Datas_i=Datas[i].size()-1;
            for (int j=0;j<end_Datas_i;++j)
            {
                QVector < TComplex > cmplxs=OtvToMtrx(i,j,2048);
                cmplxs=ObrFFF(cmplxs,-1);

                for (int k=18;k<23;++k)
                {
                    cmplxs[k].real=0;
                    cmplxs[k].img=0;
                }

                for (int k=2025;k<2030;++k)
                {
                    cmplxs[k].real=0;
                    cmplxs[k].img=0;
                }

                cmplxs=ObrFFF(cmplxs,1);

                CmplxToNumbEnd(i, cmplxs);
            }
        }

}

void ObrEGG::PreSaveObr(TAllDatas &CpDatas)
{
    for (int i=0;i<CpDatas.size();++i)
        if (CpDatas[i].size()>0)
            for (int j=0;j<CpDatas[i][0].size();++j)
                for (int k=0;k<(CpDatas[i].size()-1);++k)
                    CpDatas[i][k][j]=0.25*CpDatas[i][k][j]/CpDatas[i][CpDatas[i].size()-1][j];

    for (int i=0;i<CpDatas.size();++i)
        if (CpDatas[i].size()>0)
            for (int k=0;k<(CpDatas[i].size()-1);++k)
                for (int j=(CpDatas[i][k].size()-1);j>=0;--j)
                    CpDatas[i][k][j]-=CpDatas[i][k][0] ;

    int end_CpDatas=CpDatas.size();
    for (int i=0;i<end_CpDatas;++i)
        if (CpDatas[i].size()>0)
        {
            int end_CpDatas_i=CpDatas[i].size()-1;
            for (int j=0;j<end_CpDatas_i;++j)
            {
                QVector < TComplex > cmplxs=OtvToMtrx(i,j,2048,CpDatas);
                cmplxs=ObrFFF(cmplxs,-1);

                for (int k=18;k<23;++k)
                {
                    cmplxs[k].real=0;
                    cmplxs[k].img=0;
                }

                for (int k=2025;k<2030;++k)
                {
                    cmplxs[k].real=0;
                    cmplxs[k].img=0;
                }

                cmplxs=ObrFFF(cmplxs,1);

                CmplxToNumbEnd(i, cmplxs,CpDatas);
            }
        }

}

void ObrEGG::SaveDatasAll(QString fname)
{
    for (int i=0;i<Datas.size();++i)
    {
        if (Datas[i].size()>0)
        {
            QFile file(fname+"_"+QString::number(i)+"_.csv");
            file.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream out(&file);

            for (int j=0;j<Datas[i][0].size();++j)
                if (use_elektrds.contains(j))
                    out << j << ";" ;
                else if (use_elektrds.contains(j-max_elektrds))
                    out << "FFT_" << j-max_elektrds << ";" ;
            out << endl ;

            for (int j=0;j<Datas[i][0].size();++j)
            {
                for (int k=0;(k<Datas[i].size())&&((use_elektrds.contains(j))||(use_elektrds.contains(j-max_elektrds)));++k)
                    if (j<Datas[i][k].size())
                        out << Datas[i][k][j] << ";" ;
//                    out << 0.25*Datas[i][k][j]/Datas[i][Datas[i].size()-1][j] << ";" ;
//                out << Datas[i][Datas[i].size()-1][j];
                out << endl;
            }
            file.close();
        }
    }

    if (Debug_out)
    {
        for (int i=0;i<Deb_Dts.size();++i)
        {
            if (Deb_Dts[i].size()>0)
            {
                QFile file(fname+"_DebugOut_"+QString::number(i)+"_.csv");
                file.open(QIODevice::WriteOnly | QIODevice::Text);
                QTextStream out(&file);

                for (int j=0;j<Deb_Dts[i][0].size();++j)
                    if (use_elektrds.contains(j))
                        out << j << ";" ;

                for (int j=0;j<Deb_Dts[i][0].size();++j)
                {
                    for (int k=0;(k<Deb_Dts[i].size())&&((use_elektrds.contains(j))||(use_elektrds.contains(j-max_elektrds)));++k)
                        if (j<Deb_Dts[i][k].size())
                            out << Deb_Dts[i][k][j] << ";" ;
                    out << endl;
                }
                file.close();
            }
        }
    }
}

void ObrEGG::SaveDatasAll(QString fname, TAllDatas &CpDatas)
{
    for (int i=0;i<CpDatas.size();++i)
    {
        if (CpDatas[i].size()>0)
        {
            QFile file(fname+"_"+QString::number(i)+"_.csv");
            file.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream out(&file);
            for (int j=0;j<CpDatas[i][0].size();++j)
            {
                for (int k=0;k<CpDatas[i].size();++k)
                    if (j<CpDatas[i][k].size())
                        out << CpDatas[i][k][j] << ";" ;
//                    out << 0.25*CpDatas[i][k][j]/CpDatas[i][CpDatas[i].size()-1][j] << ";" ;
//                out << CpDatas[i][CpDatas[i].size()-1][j];
                out << endl;
            }
            file.close();
        }
    }
}


void ObrEGG::GetData(int NumbCom, TDatas datas, int numb)
{
    int k=k_tik++;
    while (k!=k_do) ;

    if (Datas.size()<=NumbCom) Datas.resize(NumbCom+1);

    if ((Deb_Dts.size()<=NumbCom)&&(Debug_out)) Deb_Dts.resize(NumbCom+1);

    if (NumbCom!=0) AddNakopl(NumbCom,datas,numb*5-4);
    else
    {

            if (Datas.size()==0) Datas.resize(1);
            if (Datas[0].size()<datas.size()) Datas[0].resize(datas.size()+1);
            for (int j=0;j<5;++j)
            {
                for (int n=0;n<datas.size();++n)
                    Datas[0][n].push_back(datas[n][j]);
                Datas[0][datas.size()].push_back(numb*5-j);
            }
    }

    ++test;

    if (!(test%crit_cop))
    {
//        for (int i=0;i<Datas.size();++i)
//            if (Datas[i].size()>0)
//                emit Sum(i, 21 , GetSum(i, 21));
        //        PreSaveObr();
        //        SaveDatasAll(def_name+"_"+QDateTime::currentDateTime().toString("yyyy.mm.dd-hh;mm;ss")+"_"+QString::number(test)+"_"+QString::number(NLampTrue));
//        PreSaveObr();
        int otn_win=NumWin();
        if (Fsave)
        {
            TAllDatas CpDatas=Datas;
            ThrSave *th_sv = new ThrSave(CpDatas,def_name+"_"+QDateTime::currentDateTime().toString("yyyy.MM.dd_hh-mm-ss")+
                                         "_"+QString::number(test)+"_"+QString::number(NLampTrue)+"_"+QString::number(otn_win) );
            connect(th_sv,SIGNAL(Done(ThrSave*)),this,SLOT(GetStatSave(ThrSave*)));
            th_sv->start();
        }
//        emit GoSave(CpDatas);
        Datas.clear();
    }
    ++k_do;
}

void ObrEGG::FastSave(TAllDatas CpDatas)
{
    TAllDatas ThCpDatas=CpDatas;
    PreSaveObr(ThCpDatas);
    SaveDatasAll(def_name+"_"+QDateTime::currentDateTime().toString("yyyy.MM.dd_hh-mm-ss")+"_"+QString::number(test)+"_"+QString::number(NLampTrue),ThCpDatas);
}

void ObrEGG::AddNakopl(int NumbCom,QVector < QVector < int > > tmpdat,int numb)
{
    int RTimeCop=TimeCop*5;
    if (Datas.size()<(NumbCom+1))
        Datas.resize(NumbCom+1);
    if (Datas[NumbCom].size()<(tmpdat.size()+1))
        Datas[NumbCom].resize(tmpdat.size()+1);

    if (Datas[NumbCom][0].size()<RTimeCop)
    {
        for (int j=0;j<5;++j)
        {
            for (int i=0;i<tmpdat.size();++i)
                Datas[NumbCom][i].push_back(tmpdat[i][j]);
            Datas[NumbCom][tmpdat.size()].push_back(1);
        }
    }
    else
    {
        for (int j=0;j<5;++j)
        {
            for (int i=0;i<tmpdat.size();++i)
                Datas[NumbCom][i][RTimeCop-numb-j]+=tmpdat[i][j];
            Datas[NumbCom][tmpdat.size()][RTimeCop-numb-j]+=1;
        }
    }

    if (Debug_out)
    {
        if (Deb_Dts.size()<=(NumbCom+1))
            Deb_Dts.resize(NumbCom+1);
        if (Deb_Dts[NumbCom].size()<(tmpdat.size()+1))
            Deb_Dts[NumbCom].resize(tmpdat.size()+1);

        for (int j=0;j<5;++j)
            for (int i=0;i<tmpdat.size();++i)
                Datas[NumbCom][i].push_back(tmpdat[i][j]);
    }
}

float ObrEGG::GetSum(int NumbCom, int stolb)
{
    float res=0;

    QVector < TComplex > cmplxs(0);


    for (int i=0;i<Datas[NumbCom][stolb].size();++i)
    {
        TComplex tmp = { 0,0 };
        tmp.real=0.25*Datas[NumbCom][stolb][i]/Datas[NumbCom][Datas[NumbCom].size()-1][i];
        cmplxs.push_back(tmp);
    }

    for (int i=(cmplxs.size()-1);i>=0;--i)
        cmplxs[i].real-=cmplxs[0].real;

    cmplxs=ObrFFF(cmplxs,-1);

    for (int k=18;k<23;++k)
    {
        cmplxs[k].real=0;
        cmplxs[k].img=0;
    }

    for (int k=2025;k<2030;++k)
    {
        cmplxs[k].real=0;
        cmplxs[k].img=0;
    }

    cmplxs=ObrFFF(cmplxs,1);

    for (int i=(cmplxs.size()-451);i>=250;--i)
        cmplxs[i].real-=cmplxs[250].real;

    for (int i=250;i<(cmplxs.size()-450);++i)
        res+=qAbs(cmplxs[i].real);

//    int gmax=0, gmin=0, n_gmax=0, n_gmin=0;
//    QSet <int> set;
//    while ((n_gmin-n_gmax)<=0)
//    {
//        if ((gmax!=0)&&(n_gmax!=0))
//            gmax=cmplxs[--n_gmax].real-cmplxs[250].real;
//        set.insert(n_gmax);
//        for (int i=250;i<(cmplxs.size()-250);++i)
//            if (((cmplxs[i].real-cmplxs[250].real)>gmax)&&(!set.contains(i)))
//                gmax=cmplxs[i].real-cmplxs[250].real;
//            else if ((cmplxs[i].real-cmplxs[250].real)<gmin) gmin=cmplxs[i].real-cmplxs[250].real;
//    }
//    res=gmax-gmin;

    return res;
}


//void ObrEGG::GetData(int NumbCom, QVector < QVector < int > > datas, int numb)
//{
//    if (Datas.size()<=NumbCom) Datas.resize(NumbCom+1);

//    for (int j=0;j<5;++j)
//    {
//        QVector < int > tmpdat(0);
//        for (int i=0; i<60000; i+=2000)
//            tmpdat.push_back(datas[i+j]);
//        if (NumbCom!=0) AddNakopl(NumbCom,tmpdat,numb*5+j-4);
//        else
//        {
//            if (Datas.size()==0) Datas.resize(1);
//            if (Datas[0].size()<tmpdat.size()) Datas[0].resize(tmpdat.size()+1);
//            for (int n=0;n<tmpdat.size();++n)
//                Datas[0][n].push_back(tmpdat[n]);
//            Datas[0][tmpdat.size()].push_back(numb*5+j-4);
//        }
//    }
//    ++test;
//}

//void ObrEGG::AddNakopl(int NumbCom,QVector < int > tmpdat,int numb)
//{
//    int RTimeCop=TimeCop*5;
//    if (Datas.size()<(NumbCom+1))
//        Datas.resize(NumbCom+1);
//    if (Datas[NumbCom].size()<(tmpdat.size()+1))
//        Datas[NumbCom].resize(tmpdat.size()+1);

//    if (Datas[NumbCom][0].size()<RTimeCop)
//    {
//        for (int i=0;i<tmpdat.size();++i)
//            Datas[NumbCom][i].push_back(tmpdat[i]);
//        Datas[NumbCom][tmpdat.size()].push_back(1);
//    }
//    else
//    {
//        for (int i=0;i<tmpdat.size();++i)
//            Datas[NumbCom][i][RTimeCop-numb]+=tmpdat[i];
//        Datas[NumbCom][tmpdat.size()][RTimeCop-numb]+=1;
//    }
//}


void ObrEGG::LoadFrmFile(QString fname, int NimbCom)
{
    if (Datas.size()<(NimbCom+1)) Datas.resize(NimbCom+1);
    Datas[NimbCom].resize(0);
    QFile file(fname);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream in(&file);
    while (!in.atEnd())
    {
        QStringList strs = in.readLine().trimmed().split(";");
        if (Datas[NimbCom].size()<(strs.size()+1))
                Datas[NimbCom].resize(strs.size()+1);
        for (int i=0; i<strs.size();++i)
            Datas[NimbCom][i].push_back(strs.at(i).toFloat());
        Datas[NimbCom][Datas[NimbCom].size()-1].push_back(1);
    }
    file.close();
}

QVector < TComplex > ObrEGG::ObrFFF(QVector < TComplex > cmplxs, int Ft_Flag)
{
    QVector < TComplex > res(0);

    int N=2048;
    float Rdat[N], Idat[N];
    int LogN=11;

    for (int i=0;i<N;++i)
    {
        Rdat[i]=cmplxs[i].real;
        Idat[i]=cmplxs[i].img;
    }

    wFFT(Rdat,Idat,N,LogN,Ft_Flag);

    TComplex tmp_cmplx;
    for (int i=0;i<N;++i)
    {
        tmp_cmplx.real=Rdat[i];
        tmp_cmplx.img=Idat[i];
        res.push_back(tmp_cmplx);
    }

    return res;
}

QVector < TComplex > ObrEGG::OtvToMtrx(int NumbCom, int Otvedenie,int N)
{
    QVector < TComplex > res(0);
    TComplex cmplx;

    for (int i=0;i<N;++i)
    {
        cmplx.real=Datas[NumbCom][Otvedenie][i];
        cmplx.img=0;
        res.push_back(cmplx);
    }

    return res;
}

QVector < TComplex > ObrEGG::OtvToMtrx(int NumbCom, int Otvedenie,int N,TAllDatas &CpDatas)
{
    QVector < TComplex > res(0);
    TComplex cmplx;

    for (int i=0;i<N;++i)
    {
        cmplx.real=CpDatas[NumbCom][Otvedenie][i];
        cmplx.img=0;
        res.push_back(cmplx);
    }

    return res;
}

void ObrEGG::SaveCmplx(QString fname, QVector < TComplex > cmplxs)
{
    QFile file(fname);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    for (int i=0;i<cmplxs.size();++i)
        out << cmplxs[i].real << ";" << cmplxs[i].img << ";" << endl;
    file.close();
}

void ObrEGG::GetStatSave(ThrSave *potok)
{
    delete potok;
}

int ObrEGG::NumWin(bool otn)
{
    int i_l=-1;
    float max_v=0;
    QVector < std::pair< int, int > > v_max(0);
    int v_otn=-1;
    int i_otn=-1;
    if (sposb==sum)
    {
        for (int i=0;i<Datas.size();++i)
        {
            if (Datas[i].size()>0) ++i_otn;

            float tmp_max=0;
            for (int j=0;j<Datas[i].size();++j)
                if ((Datas[i][j].size()>0)&&(use_elektrds.contains(j)))
                    tmp_max+=GetSum(i, j);

            if (tmp_max>max_v)
            {
                i_l=i;
                max_v=tmp_max;
                v_otn=i_otn;
            }
        }
    } if (sposb==golos)
    {
        for (int j=0;j<max_elektrds;++j)
        {
            if ((use_elektrds.contains(j)))
            {
                float tmp_max=0;
                i_otn=-1;
                int tmp_i_l, tmp_i_o;
                for (int i=0;i<Datas.size();++i)
                    if (Datas[i].size()>0)
                    {
                        ++i_otn;
                        float tmp=GetSum(i, j);
                        if (tmp>tmp_max)
                        {
                            tmp_max=tmp;
                            tmp_i_l=i;
                            tmp_i_o=i_otn;
                        }
                    }
                std::pair <int,int> loc(tmp_i_l,tmp_i_o);
                v_max.push_back(loc);
            }

        }

        int n_w=0;
        int b_c=0;
        for (int i=0;i<v_max.size();++i)
        {
            int tmp_c=0;
            qCount(v_max,v_max[i],tmp_c);
            if (tmp_c>b_c)
            {
                b_c=tmp_c;
                n_w=i;
            }
        }


        i_l=v_max[n_w].first;
        v_otn=v_max[n_w].second;
    }

    emit SendTrWint(i_l);
    emit SendOtWint(v_otn);

    if (otn) return v_otn;
    else return i_l;
}

void ObrEGG::setNumElektrds(int nel)
{
    if (nel>=11) ++nel;
    for (int i=0;i<nel;++i)
        if (i!=10)
            use_elektrds.insert(i);
}

void ObrEGG::setFsave(bool fs)
{
    Fsave=fs;
}

void ObrEGG::SetSposb(sps_vib sps)
{
    sposb=sps;
}

void ObrEGG::setDebug(bool deb)
{
    Debug_out=deb;
}
