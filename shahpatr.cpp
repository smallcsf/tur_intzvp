﻿#include "shahpatr.h"

ShahPatr::ShahPatr(QWidget *parent) :
    QWidget(parent)
{
}

ShahPatr::ShahPatr(QString fimg_1, QString fimg_2, int NLamp, QWidget *parent) :
    QWidget(parent)
{
    numb_img=0;
    imgs.push_back(QPixmap(fimg_1));
    imgs.push_back(QPixmap(fimg_2));
    setWindowFlags(Qt::ToolTip);

//    QWidget *scr = QApplication::desktop()->screen(0); // 0 - это номер экрана (если он один то всегда 0)
//    int scrWidth = scr->width(); // ширина экрана
//    int scrHeight = scr->height(); // высота экрана

    int scrWidth = 800; // ширина экрана
    int scrHeight = 600; // высота экрана

    for (int i=0;i<imgs.size();++i)
            imgs[i]=imgs[i].scaled(scrWidth/3,scrHeight/3,Qt::KeepAspectRatio);
    resize(imgs[0].size());

    int xpos, ypos;

    switch (NLamp)
    {
    case 1:
        xpos=scrWidth-imgs[0].width();
        ypos=0;
        break;
    case 2:
        xpos=0;
        ypos=0;
        break;
    case 3:
        xpos=0;
        ypos=scrHeight-imgs[0].height();
        break;
    case 4:
        xpos=scrWidth-imgs[0].width();
        ypos=scrHeight-imgs[0].height();
        break;
    case 10:
        xpos=scrWidth;
        ypos=scrHeight;
        break;
    default:
        xpos=ypos=0;
    }

    move(xpos,ypos);

}

void ShahPatr::paintEvent(QPaintEvent *event)
{
    QPainter pain(this);
        pain.drawPixmap(0,0,imgs[numb_img]);
    pain.end();
}

void ShahPatr::SetNumbImg(int numb)
{
    if ( (numb>=0) && (numb <imgs.size()) )
        numb_img=numb;
}

void ShahPatr::NextPtrn()
{
    numb_img=(numb_img+1)%2;
}
