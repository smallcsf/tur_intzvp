﻿#include "egg_pure.h"

egg_pure::egg_pure(QString flname,QObject *parent) :
    QObject(parent), flibname(flname)
{
    libdll.setFileName(flibname);
    libdll.load();
    SetErrorFunction = (TSetErrorFunction)(libdll.resolve("SetErrorFunction"));
    SwitchOnPid = (TSwitchOnPid)(libdll.resolve("SwitchOnPid"));
    SwitchOn = (TSwitchOn)(libdll.resolve("SwitchOn"));
    SwitchOff = (TSwitchOff)(libdll.resolve("SwitchOff"));
    SetFrecEEG = (TSetFrecEEG)(libdll.resolve("SetFrecEEG"));
    ReceiveINFORMATION = (TReceiveINFORMATION)(libdll.resolve("ReceiveINFORMAHION"));
    OnStopReceive = (TOnStopReceive)(libdll.resolve("OnStopReceive"));
    SetTransmitEEG = (TSetTransmitEEG)(libdll.resolve("SetTransmitEEG"));
//    SetErrorFunction(&(func_user_error));
//    if (SetTransmitEEG) SetTransmitEEG(func_user_eeg);
}

//egg_pure::~egg_pure()
//{

//}

void egg_pure::func_user_error(unsigned int  error,unsigned int  type_error)
{

}

void egg_pure::func_user_eeg(int *buffer, long length, int time_on_bus)
{

}
