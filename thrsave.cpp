﻿#include "thrsave.h"

ThrSave::ThrSave(bool obr,QSet < int > u_e, int m_e,TAllDatas Datas,QString fname,QObject *parent) :
    QThread(parent),Dts(Datas),fnam(fname),max_elektrds(m_e),use_elektrds(u_e),
    obrabotka(obr)
{
}

void ThrSave::PreSaveObr()
{
    for (int i=0;i<Dts.size();++i)
        if (Dts[i].size()>0)
            for (int j=0;j<Dts[i][0].size();++j)
                for (int k=0;k<(Dts[i].size()-1);++k)
                    Dts[i][k][j]=0.25*Dts[i][k][j]/Dts[i][Dts[i].size()-1][j];

    for (int i=0;i<Dts.size();++i)
        if (Dts[i].size()>0)
            for (int k=0;k<(Dts[i].size()-1);++k)
                for (int j=(Dts[i][k].size()-1);j>=0;--j)
                    Dts[i][k][j]-=Dts[i][k][0] ;

    int end_Dts=Dts.size();
    for (int i=0;i<end_Dts;++i)
        if (Dts[i].size()>0)
        {
            int end_Dts_i=Dts[i].size()-1;
            for (int j=0;j<end_Dts_i;++j)
            {
                QVector < TComplex > cmplxs=OtvToMtrx(i,j,2048);
                cmplxs=ObrFFF(cmplxs,-1);

                for (int k=18;k<23;++k)
                {
                    cmplxs[k].real=0;
                    cmplxs[k].img=0;
                }

                for (int k=2025;k<2030;++k)
                {
                    cmplxs[k].real=0;
                    cmplxs[k].img=0;
                }

                cmplxs=ObrFFF(cmplxs,1);

                CmplxToNumbEnd(i, cmplxs);
            }
        }

}

QVector < TComplex > ThrSave::OtvToMtrx(int NumbCom, int Otvedenie,int N)
{
    QVector < TComplex > res(0);
    TComplex cmplx;

    for (int i=0;i<N;++i)
    {
        cmplx.real=Dts[NumbCom][Otvedenie][i];
        cmplx.img=0;
        res.push_back(cmplx);
    }

    return res;
}

void ThrSave::CmplxToNumbEnd(int numb, QVector < TComplex > cmplxs)
{
    int last_id = (Dts[numb].size());
    Dts[numb].resize(last_id+1);
    for (int i=0;i<cmplxs.size();++i)
        Dts[numb][last_id].push_back(cmplxs[i].real);
}

void ThrSave::SaveDatasAll()
{
    if (obrabotka)
    {
        use_elektrds.insert(max_elektrds-1);
        for (int i=0;i<Dts.size();++i)
        {
            if (Dts[i].size()>0)
            {
                QFile file(fnam+"_"+QString::number(i)+"_.csv");
                file.open(QIODevice::WriteOnly | QIODevice::Text);
                QTextStream out(&file);

                for (int k=0;k<Dts[i].size();++k)
                    if (use_elektrds.contains(k))
                        out << k << ";" ;
                    else if (use_elektrds.contains(k-max_elektrds))
                        out << "FFT_" << k-max_elektrds << ";" ;
                out << endl ;

                for (int j=0;j<Dts[i][0].size();++j)
                {
                    for (int k=0;k<Dts[i].size();++k)
                        if ( (j<Dts[i][k].size())&&((use_elektrds.contains(k))||(use_elektrds.contains(k-max_elektrds))) )
                            out << Dts[i][k][j] << ";" ;
                    out << endl;
                }
                file.close();
            }
        }
    } else
    {
        SaveDebugAll();

        const int Eho=2500;
        for (int i=1;i<Dts.size();++i)
        {
            if (Dts[i].size()>0)
            {
                QFile file(fnam+"_"+QString::number(i)+"_Dnakop_.csv");
                file.open(QIODevice::WriteOnly | QIODevice::Text);
                QTextStream out(&file);

                for (int k=0;k<Dts[i].size();++k)
                    if (use_elektrds.contains(k))
                        for (int n=0;n<std::ceil(1.0*Dts[i][k].size()/Eho);++n)
                            out << k << ";" ;

                out << endl ;

                for (int j=0;j<Eho;++j)
                {
                    for (int k=0;k<Dts[i].size();++k)
                        if ( (j<Dts[i][k].size())&&(use_elektrds.contains(k)) )
                            for (int n=0;n<std::ceil(1.0*Dts[i][k].size()/Eho);++n)
                                if ((j+n*Eho)<Dts[i][k].size())
                                    out << Dts[i][k][j+n*Eho] << ";" ;
                    out << endl;
                }
                file.close();
            }
        }
    }
}

void ThrSave::SaveDebugAll()
{
    QFile file(fnam+"_DALL_.csv");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);

    for (int i=0;i<Dts.size();++i)
        for (int k=0;k<Dts[i].size();++k)
            if (use_elektrds.contains(k))
                out << i << "__" << k << ";" ;
    out << endl;

    QVector < QVector < int > > indx(Dts.size());
    for (int i=0;i<indx.size();++i)
    {
        indx[i].resize(Dts[i].size());
        for (int k=0;k<indx[i].size();++k)
            indx[i][k]=0;
    }

    for (int j=0;j<Dts[0][0].size();++j)
    {
        for (int i=0;i<Dts.size();++i)
            for (int k=0;k<Dts[i].size();++k)
                if (use_elektrds.contains(k))
                    if (!i)
                    {
                        out << Dts[i][k][j] << ";" ;
                    } else if (indx[i][k]<Dts[i][k].size())
                    {
                        if ( ( (j<(Dts[0][0].size()-5))&&(indx[i][k]<(Dts[i][k].size()-5))&&(Dts[0][k][j]==Dts[i][k][indx[i][k]])&&(Dts[0][k][j+1]==Dts[i][k][indx[i][k]+1])
                               &&(Dts[0][k][j+2]==Dts[i][k][indx[i][k]+2])&&(Dts[0][k][j+3]==Dts[i][k][indx[i][k]+3])&&(Dts[0][k][j+4]==Dts[i][k][indx[i][k]+4])&&(Dts[0][k][j+5]==Dts[i][k][indx[i][k]+5]))
                             ||((j>4)&&(indx[i][k]>4)&&(Dts[0][k][j]==Dts[i][k][indx[i][k]])&&(Dts[0][k][j-1]==Dts[i][k][indx[i][k]-1]))
                             &&(Dts[0][k][j-2]==Dts[i][k][indx[i][k]-2])&&(Dts[0][k][j-3]==Dts[i][k][indx[i][k]-3])&&(Dts[0][k][j-4]==Dts[i][k][indx[i][k]-4])&&(Dts[0][k][j-5]==Dts[i][k][indx[i][k]-5]))
                        {
                            out << Dts[i][k][indx[i][k]] ;
                            ++indx[i][k];
                        }
                        out << ";" ;
                    }
        out << endl;
    }
    file.close();
}


QVector < TComplex > ThrSave::ObrFFF(QVector < TComplex > cmplxs, int Ft_Flag)
{
    QVector < TComplex > res(0);

    const int N=2048;
    float Rdat[N], Idat[N];
    int LogN=11;

    for (int i=0;i<N;++i)
    {
        Rdat[i]=cmplxs[i].real;
        Idat[i]=cmplxs[i].img;
    }

    wFFT(Rdat,Idat,N,LogN,Ft_Flag);

    TComplex tmp_cmplx;
    for (int i=0;i<N;++i)
    {
        tmp_cmplx.real=Rdat[i];
        tmp_cmplx.img=Idat[i];
        res.push_back(tmp_cmplx);
    }

    return res;
}

void ThrSave::run()
{
    if (obrabotka) PreSaveObr();
    SaveDatasAll();
    emit Done(this);
    this->quit();
}
