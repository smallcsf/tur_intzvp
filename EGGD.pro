#-------------------------------------------------
#
# Project created by QtCreator 2012-11-04T16:55:56
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = HW
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    eggdll.cpp \
    obregg.cpp \
    migalo.cpp \
    myfft.cpp \
    shahpatr.cpp \
    mig_shp.cpp \
    nki.cpp \
    thrsave.cpp \
    egg_virt.cpp \
    egg_pure.cpp \
    nki_p300.cpp

HEADERS  += mainwindow.h \
    eggdll.h \
    obregg.h \
    migalo.h \
    myfft.h \
    shahpatr.h \
    mig_shp.h \
    nki.h \
    thrsave.h \
    egg_virt.h \
    egg_types.h \
    egg_pure.h \
    nki_p300.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    NS4M.dll \
    EGGD.pro.user \
    EEG4DLL.DLL

#include(C:\QtSDK\mylibs\qextserialport\src\qextserialport.pri)
