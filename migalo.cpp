﻿#include "migalo.h"

Migalo::Migalo(QObject *parent) :
    QObject (parent)
{
    reset();
}

void Migalo::SetComPort(int NComPort)
{
    PortName="\\\\.\\COM"+QString::number(NComPort);
}


void Migalo::setRezhimOdin()
{
    rezh=odin;
}

void Migalo::setRezhimGrup(int size_grps)
{
    s_grp=size_grps;
    rezh=grup;
}

void Migalo::reset()
{
    const int Size_Lamps=25;
    Lamps.resize(Size_Lamps);
    for (int i=0;i<Size_Lamps;++i)
    {
        Lamps[i].rand=false;
        Lamps[i].period=0;
    }
    loff.clear();
    och_rand.clear();
    max_rand=0;
    min_rand=0;
    r_lamp=0;
    rezh=odin;
    s_grp=0;
    time_on=50;
    connect(this,SIGNAL(Migai(int)),this,SLOT(DoMigOn(int)));
}

void Migalo::SetLampMig(int NLamp, int period)
{
    if ((NLamp>=1)&&(NLamp<Lamps.size()))
    {
        if (period>0)
            Lamps[NLamp].period=period;
        else if (period==-1)
        {
            och_rand.enqueue(NLamp);
            Lamps[NLamp].rand=true;
        }
    }
}

void Migalo::DoMigOn(int NLamp)
{
    SetVLine(NLamp,1);
    emit Mignulo(NLamp);
    loff.enqueue(NLamp);
    QTimer::singleShot(time_on, this, SLOT(DoMigOff()));
}

void Migalo::DoMigOff()
{
    int NLamp=loff.dequeue();
    SetVLine(NLamp,0);
}

void Migalo::SetVLine(int line, int V)
{
    QFile file(PortName);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    out << "$KE,WR,"+QString::number(line)+","+QString::number(V)+"\r\n" ;
    file.close();
}

void Migalo::timerEvent(QTimerEvent *event)
{
    for (int i=1;i<Lamps.size();++i)
        if (event->timerId()==Lamps[i].t_id)
            emit Migai(i);

    if (event->timerId()==Lamps[r_lamp].t_id)
    {
        killTimer(Lamps[r_lamp].t_id);

        if (rezh==odin)
        {
            if (och_rand.isEmpty()) MakeRandGrpQ();
            int next=och_rand.dequeue();
            emit Migai(next);
//            och_rand.enqueue(next);
        } else if (rezh==grup)
        {
            for (int i=0;i<s_grp;++i)
            {
                if (och_rand.isEmpty()) MakeRandGrpQ();
                emit Migai(och_rand.dequeue());
            }
        }

        Lamps[r_lamp].period=getRandTime();
        Lamps[r_lamp].t_id=startTimer(Lamps[r_lamp].period);
    }
}

int Migalo::getRandTime()
{
    int res;
    qsrand(time(NULL)*1000*qrand());
    res=min_rand+qrand()%(max_rand-min_rand);
    return res;
}


void Migalo::MakeRandGrpQ()
{
    och_rand.clear();
    QVector < int > vib(0);

    for (int i=1;i<Lamps.size();++i)
        if (Lamps[i].rand)
            vib.push_back(i);

    qsrand(time(NULL)*1000*qrand());

    QSet < int > was;
    for (int i=0;i<vib.size();++i)
    {
        int next=qrand()%vib.size();
        while (was.contains(next))
            next=qrand()%vib.size();
        och_rand.enqueue(vib[next]);
        was.insert(next);
    }
}

void Migalo::stat(int t_max_rand)
{
    const int t_min_rand=200;
    max_rand=t_max_rand;
    min_rand=t_min_rand;

    if (och_rand.size()>0)
        Lamps[r_lamp].period=getRandTime();

    for (int i=0;i<Lamps.size();++i)
        if (Lamps[i].period>0)
            Lamps[i].t_id=startTimer(Lamps[i].period);

//    if (rezh==grup)
    MakeRandGrpQ();
}

void Migalo::stop()
{
    for (int i=0;i<Lamps.size();++i)
    {
        if (Lamps[i].period!=0)
        {
            killTimer(Lamps[i].t_id);
            SetVLine(i,0);
            Lamps[i].period=0;
        }
        if (Lamps[i].rand)
            Lamps[i].rand=false;
    }

    och_rand.clear();
}

void Migalo::SendZeroNLamp()
{
    emit Mignulo(0);
}
