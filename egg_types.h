#ifndef EGG_TYPES_H
#define EGG_TYPES_H

typedef unsigned char  byte; // 1byte
typedef unsigned short  WORD; // 2bytes
typedef unsigned long  DWORD; //4bytes
typedef short smallint;

typedef struct
{
char    name_device[31];        //- ��� �������
long    number_device;          //- ��������� �����
long    version_device;         //- ����� ������ ������� � ������� ����� � ��� ������� � ������� �����
long    alarm_device;           //- ������ � ������ �������
long    opornoe;                //- ��� �������� ���������� +2.5�
long    U_high_plus;            //- ��� ��������� �������
long    U_high_minus;           //- ��� ��������� �������
long    U_low_plus;             //- ��� ��������� �������
long    U_low_minus;            //- ��� ��������� �������
long    temperature;            //- ��� ������ �����������
long    spread;                 //- ��� ������ �������
long    chanel_E1;              //- ��� ����
long    chanel_E2;              //- ��� ����
} type_buffer_information;

typedef struct
{
smallint address;        // 1 ��� USB
int  dma;                // handle ��� USB
byte irq;                // ���������� �������� �� USB � ���� ������� versia (�������� ������ ������ ������ ������)
int address_BufferDMA;  // ��������� ����� ��� USB �������
int address_driver;     // ��� ������� ��� ������ 6(���4�) ��� USB  ��� 0 ��� ������ ������
int version;             // ������ ������� ����������� �� ������ �������
}  type_date_port;

//struct buffer_egg
//{
//    int buf[60000];
//};

typedef void (*TSetTransmitEEG)(void (*func)(int *buffer, long length, int time_on_bus));
typedef void (*TOnStopReceive)();
typedef type_buffer_information* (*TReceiveINFORMATION)();
typedef void (*TSetFrecEEG)(int frec_descreta);
typedef void (*TSwitchOff)();
typedef type_date_port* (*TSwitchOn) (int FPort);
typedef type_date_port*  (*TSwitchOnPid) (WORD  pid, WORD serial_number);
typedef void (*TSetErrorFunction) (void (*func)(unsigned int  error,unsigned int  type_error));

#endif // EGG_TYPES_H
