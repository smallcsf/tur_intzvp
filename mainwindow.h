﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QObject>
#include <QtCore>
#include <QtWidgets/QMainWindow>
#include "eggdll.h"
#include "obregg.h"
#include "migalo.h"
#include "shahpatr.h"
#include "mig_shp.h"
#include "nki.h"
#include "nki_p300.h"
//#include <QFileDialog>
#include <QtWidgets/QFileDialog>
#include <QTimerEvent>
#include <QTimer>
#include <QWaitCondition>
#include <QtWidgets/QDesktopWidget>
#include <QDir>


typedef QVector<QVector<int> > TDatas;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    int t_id;
    QDateTime t_st;
    int find_max(int n, double x[]);
public:
//    EGGDll myegg;
    ObrEGG myobr;
    Migalo mymig;
    mig_shp mymig_shp;
    NKI mynki;
    nki_p300 nki_p3;
    QVector < double > mtrx_res;
    int count_lmps;
    int last_lamp;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void GetSaveStatus(int prcs);
    void GetEggErrors(unsigned int  error,unsigned int  type_error);
    void GetSum(int NumbCom, int stolb, float sum);
    void DoSrv();

    void GetTestInfo(int test);

    void on_puBuStatEGG_clicked();

    void on_puBuStopEGG_clicked();

    void on_puBuConnect_clicked();

    void on_chBxAllOnOff_clicked();

    void on_puBuSaveAll_clicked();

    void on_puBuMig_clicked();

    void on_puBuStopMig_clicked();

    void on_puBuSmthDir_clicked();

    void on_puBusmth_clicked();

    void on_puBuSaveBlt_clicked();

    void on_puButest_clicked();

    void on_puBuMigShp_clicked();

    void on_puBuStopMigShp_clicked();

    void on_puBuConnetcSums_clicked();

    void on_spBxNLampTrue_editingFinished();

    void on_puBuSmthSmth_clicked();

    void on_chHxFSave_clicked();

    void on_chkBxSpSum_clicked();

    void on_chkBxSpGolos_clicked();

    void on_spBxCountEtcrds_editingFinished();

    void on_chkBxAmp_clicked();

    void on_chkBxP300_clicked();

    void on_chkBxSpSumGolos_clicked();

private:
    Ui::MainWindow *ui;

protected:
    void timerEvent(QTimerEvent *event);
};

#endif // MAINWINDOW_H
