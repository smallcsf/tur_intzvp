﻿#ifndef MIG_SHP_H
#define MIG_SHP_H

#include "migalo.h"
#include "shahpatr.h"

class mig_shp : public Migalo
{
    Q_OBJECT
private:
    QVector < ShahPatr* > VLines;
    void SetVLine(int line, int V);
public:
    explicit mig_shp(QObject *parent = 0);
    void SetLampMig(int NLamp, int period);

signals:
    
public slots:
    
};

#endif // MIG_SHP_H
