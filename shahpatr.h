﻿#ifndef SHAHPATR_H
#define SHAHPATR_H

#include <QtWidgets/qwidget.h>
#include <QPixmap>
#include <QPainter>
#include <QtWidgets/QDesktopWidget>
//#include <QApplication>
//#include <QApplication>
#include <QVector>

class ShahPatr : public QWidget
{
    Q_OBJECT
private:
    QVector < QPixmap > imgs;
    int numb_img;
public:
    explicit ShahPatr(QWidget *parent = 0);
    ShahPatr(QString fimg_1, QString fimg_2, int NLamp = 0, QWidget *parent = 0);
    void SetNumbImg(int numb);
    void NextPtrn();

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event);
};

#endif // SHAHPATR_H
