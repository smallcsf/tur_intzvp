﻿#ifndef OBREGG_H
#define OBREGG_H

#include <QObject>
#include <QVector>
#include <QFile>
#include <QTextStream>
#include <QStringList>
#include "myFFT.h"
#include <QSet>
#include <QDateTime>
#include "thrsave.h"
#include <QDebug>

typedef QVector<QVector<int> > TDatas;
typedef QVector < QVector < QVector < float > > > TAllDatas;

//struct TComplex
//{
//    float real;
//    float img;
//};

enum sps_vib {sum, golos, sum_golos};
enum tp_nki {amp, p300};

class ObrEGG : public QObject
{
    Q_OBJECT
private:
    QVector < QVector < QVector < float > > > Datas;
    QVector < QVector < QVector < float > > > Deb_Dts;
    QString def_name;
    int NLampTrue;
    void reset();
    void AddNakopl(int NumbCom,QVector < QVector < int > > tmpdat,int numb);
    void CmplxToNumbEnd(int numb, QVector < TComplex > cmplxs);
    const int max_elektrds;
    int TimeCop;
    int test;
    int k_tik;
    int k_do;
    int crit_cop;
    QSet < int > use_elektrds;
    float GetSum(int NumbCom, int stolb);
    bool Fsave;
    bool Debug_out;
    sps_vib sposb;
    tp_nki tnki;
public:
    void setDebug(bool deb);
    void SetSposb(sps_vib sps);
    void setTNKI(tp_nki tp);
    void setNumElektrds(int nel);
    void setFsave(bool fs);
    void setCritCop(int cr_cp,int n_lmps);
    void SetDName(QString dname);
    void setNLamp(int nlmp);
    void SetTimeCop(int TC);
    explicit ObrEGG(QObject *parent = 0);
    void SaveDatas_0(QString fname);
    void SaveDatasAll(QString fname);
    void LoadFrmFile(QString fname, int NimbCom);
    void PreSaveObr();
    static QVector < TComplex > ObrFFF(QVector < TComplex > cmplxs, int Ft_Flag, int okno_st = 0, int okno_end = -1);
    QVector < TComplex > OtvToMtrx(int NumbCom, int Otvedenie,int N);
    std::pair <int,int> NumWin(int i_st = 1, int i_end = -1, bool otn = false);

    static void SaveCmplx(QString fname, QVector < TComplex > cmplxs);
    static void SaveDatasAll(QString fname,TAllDatas &CpDatas);
    static void PreSaveObr(TAllDatas &CpDatas);
    static QVector < TComplex > OtvToMtrx(int NumbCom, int Otvedenie,int N, TAllDatas &CpDatas);
    static void CmplxToNumbEnd(int numb, QVector < TComplex > cmplxs, TAllDatas &CpDatas);
    
signals:
    void Sum(int NumbCom, int stolb, float sum);
    void GoSave(TAllDatas CpDatas);
    void SendTrWint(int NumbCom);
    void SendOtWint(int NumbCom);
    void SendStSavin(int st);
    void SendAddWin(int aw);
    
public slots:
    void GetData(int NumbCom, TDatas datas, int numb);
    void FastSave(TAllDatas CpDatas);
    void GetStatSave(ThrSave *potok);
};

#endif // OBREGG_H
