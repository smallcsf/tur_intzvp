﻿#ifndef EGG_PURE_H
#define EGG_PURE_H

#include <QObject>
#include <QLibrary>
#include <QString>
#include "egg_types.h"
#include "egg_virt.h"

class egg_pure : public QObject, public Egg_virt
{
    Q_OBJECT

private:
    QString flibname;
    QLibrary libdll;
    TSetTransmitEEG SetTransmitEEG;
    TSetErrorFunction SetErrorFunction;
    TSwitchOnPid SwitchOnPid;
    TSwitchOn SwitchOn;
    TSwitchOff SwitchOff;
    TSetFrecEEG SetFrecEEG;
    TReceiveINFORMATION ReceiveINFORMATION;
    TOnStopReceive OnStopReceive;
public:
    explicit egg_pure(QString flname = "EEG4DLL", QObject *parent = 0);

    void func_user_error(unsigned int  error,unsigned int  type_error);
    void func_user_eeg(int *buffer, long length, int time_on_bus);

//    ~egg_pure();
signals:
    
public slots:
    
};

#endif // EGG_PURE_H
