#ifndef EGG_VIRT_H
#define EGG_VIRT_H

class Egg_virt
{
public:
//    Egg_virt();
    virtual void func_user_error(unsigned int  error,unsigned int  type_error)=0;
    virtual void func_user_eeg(int *buffer, long length, int time_on_bus)=0;
//    virtual ~Egg_virt() =0;
};

#endif // EGG_VIRT_H
