﻿#include "mig_shp.h"

mig_shp::mig_shp(QObject *parent) :
    Migalo(parent)
{
}

void mig_shp::SetVLine(int line, int V)
{
    if (V) VLines[line]->NextPtrn();
    VLines[line]->repaint();
}

void mig_shp::SetLampMig(int NLamp, int period)
{
    if ((NLamp>=0)&&(NLamp<24))
    {
        if (period!=-1)
            Lamps[NLamp].period=period;
        else
        {
            qsrand(time(NULL)*1000*NLamp);
            Lamps[NLamp].period=200+qrand()%1000;
            Lamps[NLamp].rand=true;
        }
    }
    if (NLamp>=VLines.size()) VLines.resize(NLamp+1);
        VLines[NLamp] = new ShahPatr("0.bmp","1.bmp",NLamp);
        VLines[NLamp]->show();
//        VLines[NLamp]->setVisible(false);
}
