﻿#include "nki_p300.h"

nki_p300::nki_p300(int grds_lev, int c_obl, int inf_obl, QObject *parent) :
    Migalo(parent),mnki(grds_lev),g_levs(grds_lev)
{
    mnki.toStat();
    mnki.setCountObls(c_obl);
    mnki.setInfoObls(inf_obl);

    int c_lmps=2*std::pow(c_obl-1,0.5);
    for (int i=1;i<=c_lmps;++i)
        SetLampMig(i,-1);
    time_on=100;
//    mnki.show();
}

void nki_p300::SetVLine(int line, int V)
{
    if (V)
    {
        if ((line>0)&&(line<=g_levs))
            for (int i=line;i<=mnki.getUsObls();i+=g_levs)
                mnki.addVid(i-1);
        else if ((line>g_levs)&&(line<=(2*g_levs)))
            for (int i=6*((line-g_levs)-1)+1;i<=6*((line-g_levs)-1)+6;++i)
                mnki.addVid(i-1);
    } else mnki.ClearVid();

    mnki.repaint();
}

void nki_p300::show()
{
    mnki.show();
}

void nki_p300::getCheck(int numb)
{
    mnki.setNumCheck(numb);
}

void nki_p300::SetNumCheck(int nch)
{
    mnki.setNumCheck(nch);
}

void nki_p300::SetRezhDeep(bool dp)
{
    mnki.SetRezhDeep(dp);
}

void nki_p300::getVid(int numb)
{
    mnki.setNumVid(numb);
}
