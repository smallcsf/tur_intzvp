﻿#ifndef NKI_H
#define NKI_H

#include <QtWidgets/qwidget.h>
#include <QPixmap>
#include <QPainter>
#include <cmath>
#include <stdlib.h>
#include <QSet>

class NKI : public QWidget
{
    Q_OBJECT
private:
    QVector < QString > obs;
    void reset();
    int count_obls;
    int num_info_obl;
    int num_check;
    int od_num_vid;
    int g_max_in_lev;
    bool no_deep;
    QSet < int > nums_vid;
    QString itog;
    QPixmap drawObls(int num_obl, int o_width, int o_heigh);
    QVector < QString > getOblsOb(int num_obl);

public:
    explicit NKI(int grds_lev=3,QWidget *parent = 0);
    void setCountObls(int cobls);
    void setInfoObls(int cobls);
    void setNumCheck(int numb);
    void setNumVid(int numb);
    void toStat(bool def_itog=true);
    void GoDeeper(int numb);
    void SetRezhDeep(bool dp);
    void ClearVid();
    void addVid(int n_obl);
    int getUsObls();
    int getInfoObl();

protected:
    void paintEvent(QPaintEvent *event);

signals:

public slots:
    void getCheck(int numb);
    void getVid(int numb);

};

#endif // NKI_H
