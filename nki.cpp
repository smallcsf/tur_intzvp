﻿#include "nki.h"

NKI::NKI(int grds_lev, QWidget *parent) :
    QWidget(parent),g_max_in_lev(grds_lev)
{
    reset();
}

void NKI::toStat(bool def_itog)
{
    const int tmp_size=40;
    char tmp_obs[tmp_size] = {'0','1','2','3','4','5','6','7','8','9',
                              'a','b','c','d','e','f','g','h','i','j','k',
                              'l','m','n','o','p','q','r','s','t','u','v',
                              'w','x','y','z','I','V','X','L'};
    for (int i=0;i<tmp_size;++i)
        obs.push_back(QString(tmp_obs[i]));
    num_check=-1;
    od_num_vid=-1;
    if (def_itog) itog="";
    else
    {
        int inf_o=num_info_obl;
        setCountObls(count_obls);
        setInfoObls(inf_o);
    }
}

void NKI::reset()
{
    toStat();
    no_deep=false;
    count_obls=0;
    num_info_obl=-1;
    nums_vid.clear();
}

void NKI::setCountObls(int cobls)
{
    count_obls=cobls;
    num_info_obl=cobls/2;
    if (no_deep)
    {
        obs.resize(cobls-1);
    }else
    {
        if ((cobls>11)&&(cobls<obs.size()))
            obs.resize(cobls-1);
        else
        {
            int n_s=std::pow(4,(cobls-1));
            if (n_s<obs.size())
                obs.resize(n_s);
        }
    }
}

void NKI::setInfoObls(int cobls)
{
    num_info_obl=cobls;
}

int NKI::getInfoObl()
{
    return num_info_obl;
}

QPixmap NKI::drawObls(int num_obl, int o_width, int o_heigh)
{
    const int pix_otstup=5;
    const int max_in_lev=4;
    QPixmap res(o_width,o_heigh);

    if (nums_vid.contains(num_obl)) res.fill(Qt::red);
    else if ((num_check==od_num_vid)&&(num_check==num_obl)) res.fill(Qt::yellow);
    else if (num_obl==num_check) res.fill(Qt::green);
    else if (num_obl==od_num_vid) res.fill(Qt::blue);
    else res.fill(Qt::gray);

    QPainter mypaint(&res);

    QVector < QString > o_obs = getOblsOb(num_obl);
    int count_lev=ceil(1.0*o_obs.size()/max_in_lev);
    int heigh_obls=o_heigh/count_lev;
    int width_obls=o_width/max_in_lev;
    if (o_obs.size()<max_in_lev)
        width_obls=o_width/o_obs.size();
    for (int l=0;l<count_lev;++l)
        for (int i=0;(i<max_in_lev)&&((max_in_lev*l+i)<o_obs.size());++i)
        {
            int xl=i*width_obls;
            int yl=l*heigh_obls;
            int xr=(i+1)*width_obls;
            int yr=(l+1)*heigh_obls;
            mypaint.setFont(QFont("Arial", std::min(heigh_obls,width_obls)/1.5));
            while (mypaint.fontMetrics().width(o_obs[max_in_lev*l+i])>(width_obls-(width_obls*0.4)))
                mypaint.setFont(QFont("Arial",mypaint.font().pointSize()-1));
//            mypaint.drawText((xl+xr)/2,(yl+yr)/2,o_obs[max_in_lev*l+i]);
            mypaint.drawText(xl+width_obls*0.2,0.0+yr-heigh_obls*0.2,o_obs[max_in_lev*l+i]);


        }
    mypaint.end();

//    res.save(QString::number(num_obl)+".png");

    return res;
}

QVector < QString > NKI::getOblsOb(int num_obl)
{
    QVector < QString > res(0);
    if (num_obl<num_info_obl)
    {
        int o_size=ceil(1.0*obs.size()/(count_obls-1));
        for (int i=(num_obl*o_size);(i<(num_obl*o_size+o_size))&&(i<obs.size());++i)
            res.push_back(obs[i]);
    } if (num_obl>num_info_obl)
    {
        int o_size=ceil(1.0*obs.size()/(count_obls-1));
        for (int i=((num_obl-1)*o_size);(i<((num_obl-1)*o_size+o_size))&&(i<obs.size());++i)
            res.push_back(obs[i]);
    }else if (num_obl==num_info_obl)
        res.push_back(itog);
    return res;
}

void NKI::paintEvent(QPaintEvent *event)
{
    QPainter pain(this);

    int count_lev=ceil(1.0*count_obls/g_max_in_lev);
    int heigh_obls=(this->height()-1)/count_lev;
    int width_obls=(this->width()-1)/g_max_in_lev;
    if (count_obls<g_max_in_lev)
        width_obls=this->width()/count_obls;
    for (int l=0;l<count_lev;++l)
        for (int i=0;(i<g_max_in_lev)&&((g_max_in_lev*l+i)<count_obls);++i)
        {
//            pain.drawPixmap(QRect(i*width_obls,l*heigh_obls,(i+1)*width_obls,(l+1)*heigh_obls),drawObls(i, this->width(), this->height(), false));
//            pain.drawRect(QRect(i*width_obls,l*heigh_obls,(i+1)*width_obls,
//                                (l+1)*heigh_obls));

//            pain.fillRect(QRect(i*width_obls,l*heigh_obls,(i+1)*width_obls,
//                                (l+1)*heigh_obls),
//                          drawObls(l*max_in_lev+i, width_obls,heigh_obls, false));
            const QPoint pRct[] = {QPoint(i*width_obls,l*heigh_obls),
                                   QPoint((i+1)*width_obls,l*heigh_obls),
                                   QPoint((i+1)*width_obls,(l+1)*heigh_obls),
                                   QPoint(i*width_obls,(l+1)*heigh_obls),
                                   QPoint(i*width_obls,l*heigh_obls)};

            QPixmap this_pix=drawObls(l*g_max_in_lev+i, width_obls,heigh_obls);
            pain.drawPixmap(QRect(pRct[0],pRct[2]),this_pix);

            pain.drawPolyline(pRct,5);


//            pain.drawLine(i*width_obls,l*heigh_obls,(i+1)*width_obls,l*heigh_obls);
//            pain.drawLine((i+1)*width_obls,l*heigh_obls,(i+1)*width_obls,(l+1)*heigh_obls);
//            pain.drawLine((i+1)*width_obls,(l+1)*heigh_obls,i*width_obls,(l+1)*heigh_obls);
//            pain.drawLine(i*width_obls,(l+1)*heigh_obls,i*width_obls,l*heigh_obls);
        }
            pain.end();
}

void NKI::setNumCheck(int numb)
{
    if ((numb>=0)&&(numb<count_obls))
    {
        if (numb>=num_info_obl)
            ++numb;

        if ((num_check==numb)&&(!no_deep))
        {
            num_check=-1;
            GoDeeper(numb);
        } else
            num_check=numb;
    }

    if (!this->isVisible())
        this->show();
    this->repaint();
}

void NKI::GoDeeper(int numb)
{
    QVector < QString > o_obs = getOblsOb(numb);
    if (o_obs.size()>1)
        obs=o_obs;
    else
    {
        itog+=o_obs[0];
        toStat(false);
    }
}

void NKI::getCheck(int numb)
{
    setNumCheck(numb);
}

void NKI::setNumVid(int numb)
{
    if (numb>=num_info_obl)
        ++numb;
    od_num_vid=numb;

    if (!this->isVisible())
        this->show();
    this->repaint();
}

void NKI::getVid(int numb)
{
    setNumVid(numb);
}

void NKI::ClearVid()
{
    nums_vid.clear();
}

void NKI::addVid(int n_obl)
{
    nums_vid.insert(n_obl);
}

int NKI::getUsObls()
{
    return (count_obls-1) ;
}

void NKI::SetRezhDeep(bool dp)
{
    no_deep=dp;
    toStat(false);
}
