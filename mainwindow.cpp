﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGraphicsView>

EGGDll myegg;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)/*, nki_p3()*/
{
    ui->setupUi(this);

    myegg.setParams("EEG4DLL");
    myegg.SetTimeCop(500);
    myobr.SetTimeCop(500);
    myegg.test=789;
    t_id=0;

    move(0,200);
    mtrx_res.clear();
    for (int i=0;i<25;i++)
        mtrx_res.push_back(0);
    count_lmps=0;
    last_lamp=-1;
}

MainWindow::~MainWindow()
{
    myegg.StopEgg();
    delete ui;
}


int MainWindow::find_max(int n, double x[])
{
    int max_i=1;
    double max=x[0];

    for (int i=1; i<n; ++i)
        if ((x[i])>max)
        {
            max=(x[i]);
            max_i=i+1;
        }

    return max_i;
}

void MainWindow::on_puBuStatEGG_clicked()
{
    connect(&myegg,SIGNAL(NewError(unsigned int,unsigned int)),this,SLOT(GetEggErrors(unsigned int,unsigned int)));
    myegg.SetEgg(); 
}

void MainWindow::on_puBuStopEGG_clicked()
{
    myegg.StopEgg();
}

void MainWindow::on_puBuConnect_clicked()
{
//    if (!QDir::exists(QCoreApplication::applicationDirPath()+"/"+ui->spBxCountEtcrds->value()))
//        QDir::mkdir(QCoreApplication::applicationDirPath()+"/"+ui->spBxCountEtcrds->value());
    myobr.setNumElektrds(ui->spBxCountEtcrds->value());


    qRegisterMetaType<TDatas>("TDatas");

    //    connect(&mymig_shp,SIGNAL(Mignulo(int)),&myegg,SLOT(AddComand(int)));
    connect(&myegg,SIGNAL(SendDatas(int, TDatas ,int)),&myobr,SLOT(GetData(int, TDatas ,int)));
    connect(&myobr,SIGNAL(SendStSavin(int)),this,SLOT(GetSaveStatus(int)));


    if (ui->chkBxAmp->isChecked())
    {
        connect(&mymig,SIGNAL(Mignulo(int)),&myegg,SLOT(AddComand(int)));
        connect(&myobr,SIGNAL(SendOtWint(int)),&mynki,SLOT(getCheck(int)));
        connect(&myobr,SIGNAL(SendAddWin(int)),&mynki,SLOT(getVid(int)));

        if (ui->chkBxDebug->isChecked())
            mymig.SendZeroNLamp();

    } else if (ui->chkBxP300->isChecked())
    {
        connect(&nki_p3,SIGNAL(Mignulo(int)),&myegg,SLOT(AddComand(int)));
        connect(&myobr,SIGNAL(SendOtWint(int)),&nki_p3,SLOT(getCheck(int)));
        connect(&myobr,SIGNAL(SendAddWin(int)),&nki_p3,SLOT(getVid(int)));

        if (ui->chkBxDebug->isChecked())
            nki_p3.SendZeroNLamp();
    }

    myobr.setDebug(ui->chkBxDebug->isChecked());
//    myegg.SendAll=ui->chBxAllOnOff->isChecked();


    ui->lbConnect->setText("Conntected");

    QString prefix="_rezh"+QString::number(ui->spBxRezhGrp->value())+"_";

    if (!QDir().exists(ui->lEdTest->text()))
        QDir().mkdir(ui->lEdTest->text());

    myobr.SetDName(QDir(QDir(ui->lEdTest->text())).path()+"/"+ui->lEdTest->text()+prefix);

    t_st=QDateTime::currentDateTime();
    t_id=startTimer(1000);
}

void MainWindow::on_chBxAllOnOff_clicked()
{
//    myegg.SendAll=ui->chBxAllOnOff->isChecked();
}

void MainWindow::on_puBuSaveAll_clicked()
{
//    myobr.SaveDatas_0(ui->lEdTest->text());
    myobr.PreSaveObr();
    myobr.SaveDatasAll(ui->lEdTest->text());
    ui->lbSave->setText("Save");
}

void MainWindow::on_puBuMig_clicked()
{
    if (ui->chkBxAmp->isChecked())
    {
        mymig.SetComPort(ui->lEdMigCom->text().toInt());
        mynki.toStat();
        last_lamp=-1;
        count_lmps=0;
        //    mymig.SetLampMig(24,100);
        //    mymig.SetLampMig(1,-1);
        //    mymig.SetLampMig(2,-1);
        //    mymig.SetLampMig(10,600);

        QStringList lmps=ui->lEdNlamps->text().trimmed().split(" ", QString::SkipEmptyParts);

        for (int i=0;i<lmps.size();++i)
        {
            QStringList lamp=lmps.at(i).split(";", QString::SkipEmptyParts);
            mymig.SetLampMig(lamp.at(0).toInt(),lamp.at(1).toInt());

            if (lamp.at(0).toInt()>last_lamp)
                last_lamp=lamp.at(0).toInt();

            ++count_lmps;
        }

        mynki.setCountObls(count_lmps+1);

        if (ui->spBxRezhGrp->value()>1)
            mymig.setRezhimGrup(ui->spBxRezhGrp->value());

        if (ui->chBxRezhNoDeep->isChecked())
            mynki.SetRezhDeep(true);

        myobr.setCritCop(ui->spBxCNakop->value(),count_lmps);
        mymig.stat();
        mynki.show();


        //    connect(&myobr,SIGNAL(Sum(int, int, float)),this,SLOT(GetSum(int, int, float)));

        //    connect(&myobr,SIGNAL(SendOtWint(int)),&mynki,SLOT(getCheck(int)));
    } else if (ui->chkBxP300->isChecked())
    {
        count_lmps=12;
        myobr.setCritCop(ui->spBxCNakop->value(),count_lmps);
        if (ui->spBxRezhGrp->value()>1)
            nki_p3.setRezhimGrup(ui->spBxRezhGrp->value());

        if (ui->chBxRezhNoDeep->isChecked())
            nki_p3.SetRezhDeep(true);

        nki_p3.show();
        nki_p3.stat();
    }

    if (ui->chkBxSpSumGolos->isChecked())
        myobr.SetSposb(sum_golos);

    ui->lbMig->setText("On");
}

void MainWindow::on_puBuStopMig_clicked()
{
    if (ui->chkBxAmp->isChecked())
        mymig.stop();
    else if (ui->chkBxP300->isChecked())
        nki_p3.stop();

    ui->lbMig->setText("Off");
    killTimer(t_id);
}

void MainWindow::on_puBuSmthDir_clicked()
{
//    mshp->SetNumbImg(1);
//    mshp->repaint();
//    mshp->setVisible(false);
//    ui->lEdSmth->setText(QFileDialog::getOpenFileName(this));
}

void MainWindow::on_puBusmth_clicked()
{
//    qsrand(time(NULL)*100000000);
//    int tmp=qrand();
//    ui->lEdSmth->setText(QString::number(ceil(1.0*2/(3-1))));
//    nki_p3.show();
//    nki_p3.stat();
    nki_p3.SetNumCheck(21);
//ui->lEdSmth->setText(QDateTime::currentDateTime().toString("hh:mm:ss"));
//    mshp = new ShahPatr("0.bmp","1.bmp",1);
//    mshp->show();
//    ShahPatr *sp = new ShahPatr("0.bmp","1.bmp",0);
//    sp->show();
//    QWidget *a = new QWidget();
//    a->show();
//    QWidget *b = new QWidget();
//    b->show();
//    QWidget *scr = QApplication::desktop()->screen(0); // 0 - это номер экрана (если он один то всегда 0)
//    int scrWidth = scr->width(); // ширина экрана
//    int scrHeight = scr->height(); // высота экрана
//    ui->lEdSmth->setText(QString::number(scrWidth));
//    mshp->setWindowFlags(Qt::ToolTip);


//    QGraphicsView *mias = new QGraphicsView;
//    mias->show();


//    QVector < TComplex > cmplxs(0);

//    myobr.LoadFrmFile(ui->lEdSmth->text(),1);
//    cmplxs=myobr.OtvToMtrx(1,21,2048);

////    const float Pi=3.14159265;
////    for (int i=0;i<2048;++i)
////    {
////        TComplex tmp = {sin(i/15.923),0};
////        cmplxs.push_back(tmp);
////    }

//    myobr.SaveCmplx(ui->lEdSmth->text()+"_preFFT.csv",cmplxs);
//    cmplxs=myobr.ObrFFF(cmplxs,-1);
//    myobr.SaveCmplx(ui->lEdSmth->text()+"_pramFFT.csv",cmplxs);

//    for (int i=18;i<23;++i)
//    {
//        cmplxs[i].real=0;
//        cmplxs[i].img=0;
//    }

//    for (int i=2025;i<2030;++i)
//    {
//        cmplxs[i].real=0;
//        cmplxs[i].img=0;
//    }

//    cmplxs=myobr.ObrFFF(cmplxs,1);
//    myobr.SaveCmplx(ui->lEdSmth->text()+"_obrFFT.csv",cmplxs);
}

void MainWindow::on_puBuSaveBlt_clicked()
{
    SaveAll(ui->lEdTest->text()+"_All.csv");
}

void MainWindow::GetEggErrors(unsigned int  error,unsigned int  type_error)
{
    ui->txEdEGGErrors->append(QString::number(error)+":"+QString::number(type_error));
}

void MainWindow::GetTestInfo(int test)
{
    ui->lbTest->setText(QString::number(test));
}

void MainWindow::on_puButest_clicked()
{
//    connect(&myegg,SIGNAL(Sendtest(int)),this,SLOT(GetTestInfo(int)));
//    connect(&mymig_shp,SIGNAL(test_info(int)),this,SLOT(GetTestInfo(int)));
//    connect(&mymig,SIGNAL(test_info(int)),this,SLOT(GetTestInfo(int)));
    connect(&myobr,SIGNAL(SendOtWint(int)),this,SLOT(GetTestInfo(int)));
    ui->lbTest->setText(QString::number(ceil(1.0*3/4)));
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    if (event->timerId()==t_id)
        ui->LbTime->setText(QString::number(t_st.secsTo(QDateTime::currentDateTime())));
}

void MainWindow::on_puBuMigShp_clicked()
{
    mymig_shp.SetLampMig(1,-1);
    mymig_shp.SetLampMig(2,-1);
//    mymig_shp.SetLampMig(1,415);
//    mymig_shp.SetLampMig(2,843);
//    mymig_shp.SetLampMig(3,150);
//    mymig_shp.SetLampMig(4,75);
    mymig_shp.stat();
    ui->lbMig->setText("On");
}

void MainWindow::on_puBuStopMigShp_clicked()
{
    mymig_shp.stop();
    ui->lbMig->setText("Off");
    killTimer(t_id);
}

void MainWindow::GetSum(int NumbCom, int stolb, float sum)
{
//    if (count_lmps==8)
//    {
        if (NumbCom>=mtrx_res.size())
            mtrx_res.resize(NumbCom+1);

        mtrx_res[NumbCom]=sum;

        //    switch (NumbCom)
        //    {
        //    case 17:
        //        ui->lbSum_1->setText(QString::number(sum));
        //        break;
        //    case 18:
        //        ui->lbSum_2->setText(QString::number(sum));
        //        break;
        //    case 19:
        //        ui->lbSum_3->setText(QString::number(sum));
        //        break;
        //    case 20:
        //        ui->lbSum_4->setText(QString::number(sum));
        //        break;
        //    case 21:
        //        ui->lbSum_5->setText(QString::number(sum));
        //        break;
        //    case 22:
        //        ui->lbSum_6->setText(QString::number(sum));
        //        break;
        //    case 23:
        //        ui->lbSum_7->setText(QString::number(sum));
        //        break;
        //    case 24:
        //        ui->lbSum_8->setText(QString::number(sum));
        //        break;
        //    }

        if (NumbCom==last_lamp)
            QTimer::singleShot(500, this, SLOT(DoSrv()));
//    }
}

void MainWindow::DoSrv()
{
    int max_i=-1;

    double *d = new double [count_lmps];

    for (int i=0,k=0;(k<count_lmps)&&(i<mtrx_res.size());++i)
        if (mtrx_res[i]>0)
        {
            d[k]=mtrx_res[i];
            ++k;
        }

    max_i=find_max(count_lmps,d);


    if ((max_i-1)!=mynki.getInfoObl()) --max_i;

    mynki.setNumCheck(max_i);
    if (!mynki.isVisible())
        mynki.show();
    else mynki.repaint();

//        switch (max_i)
//        {
//        case 1:
//            ui->rBuWin_1->setChecked(true);
//            break;
//        case 2:
//            ui->rBuWin_2->setChecked(true);
//            break;
//        case 3:
//            ui->rBuWin_3->setChecked(true);
//            break;
//        case 4:
//            ui->rBuWin_4->setChecked(true);
//            break;
//        case 5:
//            ui->rBuWin_5->setChecked(true);
//            break;
//        case 6:
//            ui->rBuWin_6->setChecked(true);
//            break;
//        case 7:
//            ui->rBuWin_7->setChecked(true);
//            break;
//        case 8:
//            ui->rBuWin_8->setChecked(true);
//            break;
//        }

//        ui->lbTest->setText(QString::number(max_i));

}

void MainWindow::on_puBuConnetcSums_clicked()
{
    connect(&myobr,SIGNAL(Sum(int, int, float)),this,SLOT(GetSum(int, int, float)));
}

void MainWindow::on_spBxNLampTrue_editingFinished()
{
    myobr.setNLamp(ui->spBxNLampTrue->value());
}

void MainWindow::on_puBuSmthSmth_clicked()
{
//    NKI *mynki = new NKI();
//    mynki.setCountObls(3);
//    mynki.setNumCheck(0);
//    if (!mynki.isVisible())
//        mynki.show();
//    else mynki.repaint();
//    myobr.NumWin();
    mynki.setNumCheck(0);
}

void MainWindow::on_chHxFSave_clicked()
{
    myobr.setFsave(ui->chHxFSave->isChecked());
}

void MainWindow::on_chkBxSpSum_clicked()
{
    if (ui->chkBxSpSum->isChecked())
    {
        ui->chkBxSpGolos->setChecked(false);
        ui->chkBxSpSumGolos->setChecked(false);
        myobr.SetSposb(sum);
    }
}

void MainWindow::on_chkBxSpGolos_clicked()
{
    if (ui->chkBxSpGolos->isChecked())
    {
        ui->chkBxSpSum->setChecked(false);
        ui->chkBxSpSumGolos->setChecked(false);
        myobr.SetSposb(golos);
    }
}

void MainWindow::on_spBxCountEtcrds_editingFinished()
{
    //
}

void MainWindow::on_chkBxAmp_clicked()
{
    if (ui->chkBxAmp->isChecked())
    {
        ui->chkBxP300->setChecked(false);
        myobr.setTNKI(amp);
    }
}

void MainWindow::on_chkBxP300_clicked()
{
    if (ui->chkBxP300->isChecked())
    {
        ui->chkBxAmp->setChecked(false);
        myobr.setTNKI(p300);
    }
}

void MainWindow::GetSaveStatus(int prcs)
{
        ui->lbSaving->setText(QString::number(ui->lbSaving->text().toInt()+prcs));
}

void MainWindow::on_chkBxSpSumGolos_clicked()
{
    if (ui->chkBxSpSumGolos->isChecked())
    {
        ui->chkBxSpSum->setChecked(false);
        ui->chkBxSpGolos->setChecked(false);
        myobr.SetSposb(sum_golos);
    }
}
