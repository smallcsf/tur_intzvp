﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGraphicsView>

EGGDll myegg;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    myegg.setParams("EEG4DLL");
    myegg.SetTimeCop(500);
    myobr.SetTimeCop(500);
    myegg.test=789;
    t_id=0;

    move(0,200);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_puBuStatEGG_clicked()
{
    connect(&myegg,SIGNAL(NewError(unsigned int,unsigned int)),this,SLOT(GetEggErrors(unsigned int,unsigned int)));
    myegg.SetEgg(); 
}

void MainWindow::on_puBuStopEGG_clicked()
{
    myegg.StopEgg();
}

void MainWindow::on_puBuConnect_clicked()
{
    qRegisterMetaType<TDatas>("TDatas");
    connect(&mymig,SIGNAL(Mignulo(int)),&myegg,SLOT(AddComand(int)));
    connect(&mymig_shp,SIGNAL(Mignulo(int)),&myegg,SLOT(AddComand(int)));
    connect(&myegg,SIGNAL(SendDatas(int, TDatas ,int)),&myobr,SLOT(GetData(int, TDatas ,int)));
    myegg.SendAll=ui->chBxAllOnOff->isChecked();
    ui->lbConnect->setText("Conntected");

    t_st=QDateTime::currentDateTime();
    t_id=startTimer(1000);
}

void MainWindow::on_chBxAllOnOff_clicked()
{
//    myegg.SendAll=ui->chBxAllOnOff->isChecked();
}

void MainWindow::on_puBuSaveAll_clicked()
{
//    myobr.SaveDatas_0(ui->lEdTest->text());
    myobr.PreSaveObr();
    myobr.SaveDatasAll(ui->lEdTest->text());
    ui->lbSave->setText("Save");
}

void MainWindow::on_puBuMig_clicked()
{
    mymig.SetComPort(ui->lEdMigCom->text().toInt());
//    mymig.SetLampMig(2,600);
    mymig.SetLampMig(1,600);
//    mymig.SetLampMig(10,600);
    mymig.stat();

    ui->lbMig->setText("On");
}

void MainWindow::on_puBuStopMig_clicked()
{
    mymig.stop();
    ui->lbMig->setText("Off");
    killTimer(t_id);
}

void MainWindow::on_puBuSmthDir_clicked()
{
//    mshp->SetNumbImg(1);
//    mshp->repaint();
//    mshp->setVisible(false);
//    ui->lEdSmth->setText(QFileDialog::getOpenFileName(this));
}

void MainWindow::on_puBusmth_clicked()
{
//    mshp = new ShahPatr("0.bmp","1.bmp",1);
//    mshp->show();
//    ShahPatr *sp = new ShahPatr("0.bmp","1.bmp",0);
//    sp->show();
//    QWidget *a = new QWidget();
//    a->show();
//    QWidget *b = new QWidget();
//    b->show();
//    QWidget *scr = QApplication::desktop()->screen(0); // 0 - это номер экрана (если он один то всегда 0)
//    int scrWidth = scr->width(); // ширина экрана
//    int scrHeight = scr->height(); // высота экрана
//    ui->lEdSmth->setText(QString::number(scrWidth));
//    mshp->setWindowFlags(Qt::ToolTip);


//    QGraphicsView *mias = new QGraphicsView;
//    mias->show();


//    QVector < TComplex > cmplxs(0);

//    myobr.LoadFrmFile(ui->lEdSmth->text(),1);
//    cmplxs=myobr.OtvToMtrx(1,21,2048);

////    const float Pi=3.14159265;
////    for (int i=0;i<2048;++i)
////    {
////        TComplex tmp = {sin(i/15.923),0};
////        cmplxs.push_back(tmp);
////    }

//    myobr.SaveCmplx(ui->lEdSmth->text()+"_preFFT.csv",cmplxs);
//    cmplxs=myobr.ObrFFF(cmplxs,-1);
//    myobr.SaveCmplx(ui->lEdSmth->text()+"_pramFFT.csv",cmplxs);

//    for (int i=18;i<23;++i)
//    {
//        cmplxs[i].real=0;
//        cmplxs[i].img=0;
//    }

//    for (int i=2025;i<2030;++i)
//    {
//        cmplxs[i].real=0;
//        cmplxs[i].img=0;
//    }

//    cmplxs=myobr.ObrFFF(cmplxs,1);
//    myobr.SaveCmplx(ui->lEdSmth->text()+"_obrFFT.csv",cmplxs);
}

void MainWindow::on_puBuSaveBlt_clicked()
{
    SaveAll(ui->lEdTest->text()+"_All.csv");
}

void MainWindow::GetEggErrors(unsigned int  error,unsigned int  type_error)
{
    ui->txEdEGGErrors->append(QString::number(error)+":"+QString::number(type_error));
}

void MainWindow::GetTestInfo(int test)
{
    ui->lbTest->setText(QString::number(test));
}

void MainWindow::on_puButest_clicked()
{
    connect(&myegg,SIGNAL(Sendtest(int)),this,SLOT(GetTestInfo(int)));
    ui->lbTest->setText("Ready!");
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    if (event->timerId()==t_id)
        ui->LbTime->setText(QString::number(t_st.secsTo(QDateTime::currentDateTime())));
}

void MainWindow::on_puBuMigShp_clicked()
{
    mymig_shp.SetLampMig(1,600);
//    mymig_shp.SetLampMig(10,600);
//    mymig_shp.SetLampMig(2,300);
//    mymig_shp.SetLampMig(3,150);
//    mymig_shp.SetLampMig(4,75);
    mymig_shp.stat();
    ui->lbMig->setText("On");
}

void MainWindow::on_puBuStopMigShp_clicked()
{
    mymig_shp.stop();
    ui->lbMig->setText("Off");
    killTimer(t_id);
}
