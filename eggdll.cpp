#include "eggdll.h"

QVector < QVector < int > > All(0);


void AddToAll(QVector < QVector < int > > datas, long length, int time_on_bus)
{
    if (All.size()==0) All.resize(32);
    for (int j=0;j<5;++j)
    {
        for (int i=0; i<30; ++i)
            All[i].push_back(datas[i][j]);
        All[30].push_back(time_on_bus);
        All[31].push_back(length);
    }
}

void SaveAll(QString fname)
{
    QFile file(fname);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    for (int j=0;j<All[0].size();++j)
    {
        for (int i=0;i<All.size();++i)
            out << All[i][j]<<";" ;
        out << endl;
    }
    file.close();
}

void func_user_eeg(int *buffer, long length, int time_on_bus)
{
    QVector < QVector < int > > tmpdat(30);
    for (int i=0;i<30;++i)
        for (int j=0;j<5;++j)
            tmpdat[i].push_back(buffer[i*2000+j]);
    if (myegg.ChekNeed())
    {
        ++myegg.BigCount;
        myegg.AddQueueDts(tmpdat);
//        myegg.PreSendDatas(tmpdat);
    }
    if (myegg.SendAll)
    {
//        AddToAll(tmpdat,length,time_on_bus);
//        if (SendAll) emit SendDatas(0, tmpdat, -1);
    }
}



void EGGDll::AddQueueDts(TDatas tmpdat)
{
    emit SendQueueDts(tmpdat);
}

void EGGDll::GetQueueDts(TDatas tmpdat)
{
    int k_loc=k_tik++;
    while ((!simafor)||(k_do!=k_loc)) ;
    if (simafor)
    {
        simafor=false;
        dts.enqueue(tmpdat);
        simafor=true;
        ++k_do;
    }
}

EGGDll::EGGDll(QObject *parent) :
    QObject(parent)
{

}

EGGDll::EGGDll(QString fname, QObject *parent) :
    QObject(parent)
{
    setParams(fname);
}

void EGGDll::setParams(QString fname)
{
    k_tik=0;
    k_do=0;
    simafor=true;
    turn=false;
    t_id=-1;
    dts.clear();
    test=345;
    BigCount=0;
    TimeCop=0;
    SendAll=false;
    connect(this,SIGNAL(SendQueueDts(TDatas)),this,SLOT(GetQueueDts(TDatas)));

    libdll.setFileName(fname);
    libdll.load();
    SetErrorFunction = (TSetErrorFunction)(libdll.resolve("SetErrorFunction"));
    SwitchOnPid = (TSwitchOnPid)(libdll.resolve("SwitchOnPid"));
    SwitchOn = (TSwitchOn)(libdll.resolve("SwitchOn"));
    SwitchOff = (TSwitchOff)(libdll.resolve("SwitchOff"));
    SetFrecEEG = (TSetFrecEEG)(libdll.resolve("SetFrecEEG"));
    ReceiveINFORMATION = (TReceiveINFORMATION)(libdll.resolve("ReceiveINFORMAHION"));
    OnStopReceive = (TOnStopReceive)(libdll.resolve("OnStopReceive"));
    SetTransmitEEG = (TSetTransmitEEG)(libdll.resolve("SetTransmitEEG"));
//    if (SetErrorFunction) SetErrorFunction(func_user_error);
//    if (SetTransmitEEG) SetTransmitEEG(func_user_eeg);
}

void EGGDll::SendNewError(unsigned int  error,unsigned int  type_error)
{
    emit NewError(error, type_error);
}

void EGGDll::SendNewDatas(int *datas)
{
//    emit SendDatas(0,datas,-1);
}

void EGGDll::SetFunc()
{
    if (SetErrorFunction) SetErrorFunction(func_user_error);
    if (SetTransmitEEG) SetTransmitEEG(func_user_eeg);
}

void func_user_error(unsigned int  error,unsigned int  type_error)
{
   myegg.SendNewError(error,type_error);
}

void EGGDll::PreSendDatas()
{
    int k_loc=k_tik++;
    while ((!simafor)||(k_do!=k_loc)) ;
    simafor=false;
    while (!dts.isEmpty())
    {
        if (!Comands.isEmpty())
        {
            TDatas datas=dts.dequeue();
            for (QList< TComand >::iterator it=Comands.begin();it!=Comands.end();++it)
                if ((*it).Count!=0)
                {
                    emit SendDatas((*it).NCom, datas, (*it).Count);
                    //                if (SendAll) emit SendDatas(0, datas, (*it).Count);
                    if ((*it).NCom!=0)
                        (*it).Count-=1;
                }
        }
        else
            dts.clear();
    }
    simafor=true;
    ++k_do;
    if (turn) QTimer::singleShot(1, this, SLOT(PreSendDatas()));

}

void EGGDll::AddComand(int NumbCom)
{
    emit Sendtest(dts.size());
    int k_loc=k_tik++;
    while ((!simafor)||(k_do!=k_loc)) ;
    if (simafor)
    {
        simafor=false;
        TComand NewCom = { NumbCom, TimeCop } ;
        Comands.push_back(NewCom);
        simafor=true;
        ++k_do;
    }
}

boolean EGGDll::ChekNeed()
{
    boolean res=false;
    if (Comands.size()>0) res=true;
    return res;
}

type_date_port* EGGDll::SetEgg()
{
    Info=SwitchOn(6);
    SetFrecEEG(1);
    SetFunc();
    turn=true;
    QTimer::singleShot(1, this, SLOT(PreSendDatas()));
//    t_id=startTimer(10);
    return Info;
}

void EGGDll::StopEgg()
{
    SwitchOff();
    turn=false;
}

void EGGDll::SetTimeCop(int TC)
{
    TimeCop=TC;
}

void EGGDll::timerEvent(QTimerEvent *event)
{
    if ((turn)&&(event->timerId()==t_id))
    {
        turn=false;
        PreSendDatas();
        turn=true;
    }
}
