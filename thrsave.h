﻿#ifndef THRSAVE_H
#define THRSAVE_H

#include <QThread>
#include <QVector>
#include <QFile>
#include <QString>
#include <QTextStream>
#include "myFFT.h"
#include <QSet>
#include <cmath>

typedef QVector < QVector < QVector < float > > > TAllDatas;

struct TComplex
{
    float real;
    float img;
};

class ThrSave : public QThread
{
    Q_OBJECT

private:
    TAllDatas Dts;
    QString fnam;
    QSet < int > use_elektrds;
    const int max_elektrds;
    bool obrabotka;
    void SaveDatasAll();
    void SaveDebugAll();
    void PreSaveObr();
    QVector < TComplex > OtvToMtrx(int NumbCom, int Otvedenie,int N);
    void CmplxToNumbEnd(int numb, QVector < TComplex > cmplxs);
    QVector < TComplex > ObrFFF(QVector < TComplex > cmplxs, int Ft_Flag);
public:
    explicit ThrSave(bool obr,QSet < int > u_e, int m_e, TAllDatas Datas, QString fname, QObject *parent = 0);
    
protected:
    void run();

signals:
    void Done(ThrSave *potok);
    
public slots:
    
};

#endif // THRSAVE_H
