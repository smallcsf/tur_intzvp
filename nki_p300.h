﻿#ifndef NKI_P300_H
#define NKI_P300_H

#include "nki.h"
#include "migalo.h"

class nki_p300 : public Migalo
{
    Q_OBJECT

private:
    void SetVLine(int line, int V);
    NKI mnki;
    int g_levs;
public:
    explicit nki_p300(int grds_lev = 6, int c_obl = 37, int inf_obl = 36, QObject *parent = 0);
    void show();
    void SetNumCheck(int nch);
    void SetRezhDeep(bool dp);
    
signals:
    
public slots:
    void getCheck(int numb);
    void getVid(int numb);
    
};

#endif // NKI_P300_H
