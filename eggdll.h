#ifndef EGGDLL_H
#define EGGDLL_H

#include <QObject>
#include "QLibrary"
#include "QString"
#include <QVector>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <ctime>
#include <windows.h>
#include <QQueue>
#include <QTimerEvent>
#include <QTimer>
#include <QWaitCondition>
#include "egg_types.h"




void func_user_error(unsigned int  error,unsigned int  type_error);
void func_user_eeg(int *buffer, long length, int time_on_bus);

struct TComand
{
    int NCom;
    int Count;
};

typedef QVector<QVector<int> > TDatas;

class EGGDll : public QObject
{
    Q_OBJECT
private:
    QLibrary libdll;
    TSetTransmitEEG SetTransmitEEG;
    TSetErrorFunction SetErrorFunction;
    TSwitchOnPid SwitchOnPid;
    TSwitchOn SwitchOn;
    TSwitchOff SwitchOff;
    TSetFrecEEG SetFrecEEG;
    TReceiveINFORMATION ReceiveINFORMATION;
    TOnStopReceive OnStopReceive;
    QList < TComand > Comands;
    void SetFunc();
    int TimeCop;
    QQueue < TDatas > dts;
    boolean turn;
    boolean simafor;
    int t_id;
    int k_tik;
    int k_do;
public:
    void AddQueueDts(TDatas tmpdat);
    int BigCount;
    void SetTimeCop(int TC);
    boolean SendAll;
    void setParams(QString fname);
    boolean ChekNeed();
    explicit EGGDll(QString fname, QObject *parent = 0);
    explicit EGGDll(QObject *parent = 0);
    void SendNewError(unsigned int  error,unsigned int  type_error);
    void SendNewDatas(int *datas);
    type_date_port* Info;
    type_date_port* SetEgg();
    void StopEgg();
    int test;
signals:
    void NewError(unsigned int  error,unsigned int  type_error);
    void SendDatas(int NumbCom, TDatas, int numb);
    void Sendtest(int test);
    void SendQueueDts(TDatas tmpdat);
public slots:
    void AddComand(int NumbCom);
    void PreSendDatas();
    void GetQueueDts(TDatas tmpdat);
protected:
    void timerEvent(QTimerEvent *event);

};

//type_date_port* SetEgg(void (*func)(int *buffer, long length, int time_on_bus));
//type_buffer_information* Info_Egg();
//void StopEgg();

extern EGGDll myegg;

void SaveAll(QString fname);



#endif // EGGDLL_H
